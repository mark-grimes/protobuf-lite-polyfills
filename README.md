# protobuf_lite_polyfills

This directory creates a CMake target called `protobuf_lite_polyfills` to help when using the protobuf-lite runtime.
Protobuf-lite is missing lots of functionality that doesn't become apparent until you try to link an executable that uses
these features, resulting in missing symbol errors. This target will add these in so that your program links.
When using "full" protobuf this target will have no functionality, so that it's safe to just add it as a dependency regardless
of your runtime.

Any source code from the protobuf repository here is from protobuf v3.12.2.

Functionality that you can add:
* GZip support.
* Well known types (aka wrappers).

Both of these will compile fine if you're using protobuf-lite but then fail to link.

## Licence

Any code I've written in this repository I release into the public domain, but there are some copies of code from the
protobuf repository which is covered by whatever their licence is.
